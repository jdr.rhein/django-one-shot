from django.urls import path
from todos.views import todo_list, edit_todo_item, create_todo_item, delete_list, todo_list_detail, create_list, edit_list

urlpatterns = [
    path('', todo_list, name='todo_list_list'),
    path('<int:id>/', todo_list_detail, name='todo_list_detail'),
    path('create/', create_list, name='todo_list_create'),
    path('<int:id>/edit/', edit_list, name='todo_list_update'),
    path('<int:id>/delete/', delete_list, name='todo_list_delete'),
    path('items/create/', create_todo_item, name='todo_item_create'),
    path('items/<int:id>/edit/', edit_todo_item, name='todo_item_update'),
]
